class SubscribersController < ApplicationController
  def new
    @subscriber = Subscriber.new
    render :new
  end

  def create
    @subscriber = Subscriber.new params.require(:subscriber).permit :name, :email
    if @subscriber.save
      # TODO: use cookies instead of flash
      flash[:subscriber_id] = @subscriber.id
      redirect_to new_payment_path
    else
      render :new
    end
  end
end
