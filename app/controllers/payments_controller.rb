class PaymentsController < ApplicationController
  def new
    # TODO: use cookies instead of flash
    @subscriber_id = flash[:subscriber_id]
    if not @subscriber_id
      redirect_to new_subscriber_path
    else
      @payment = Payment.new
      render :new
    end
  end

  def create
    # TODO: implement bank slip payment
    Iugu.api_key = "52dc81f689986471896c07e68713fe58"
    @payment = Payment.new params.require(:payment).permit :token, :subscriber_id
    if @payment.save
      charge = Iugu::Charge.create({
        token: @payment.token,
        email: @payment.subscriber.email,
        items: [
          {
            description: "Inscrição Bootcamp Gama Academy",
            quantity: "1",
            price_cents: "3000"
          }
        ]
      })
      if charge and charge.success
        render "success"
      else
        render "fail"
      end
    else
      render :new
    end 
  end
end
