Rails.application.routes.draw do
    get '/inscricoes' => 'subscribers#new', as: :new_subscriber
    post '/inscricoes/cadastrar' => 'subscribers#create', as: :create_subscriber
    get '/checkout' => 'payments#new', as: :new_payment
    post '/checkout/pagar' => 'payments#create', as: :create_payment
    root 'home#index'
end
