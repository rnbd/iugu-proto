class CreatePayments < ActiveRecord::Migration
  def change
    create_table :payments do |t|
      t.string :token
      t.boolean :is_approved
      t.integer :subscriber_id

      t.timestamps null: false
    end
  end
end
